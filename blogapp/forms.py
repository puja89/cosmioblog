from django import forms 
from .models import Blogger,Article,Comment
from django.contrib.auth.forms import UserCreationForm

from django.forms import ModelForm
from django.contrib.auth.models import User


class CreateUserForm(UserCreationForm):
	class Meta:
		model=User

		fields=["username","email","password1","password2"]


class BlogForm(forms.ModelForm):
    images=forms.FileField(widget=forms.FileInput(attrs={
        "multiple":True}))
    
    class Meta:

        model = Article
        fields = ['title', 'images','slug', 'content', 'views']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["text"]
        widgets = {
            "text": forms.Textarea(attrs={
                "rows": 3,
                "class": "form-control",
                "placeholder": "Write your Comment"
            })
        }        