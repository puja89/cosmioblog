from django.urls import path
from .views import home_view,registration_view,login_view
from .views import create_view,detail_view,logout_view,comment_view




app_name = 'blogapp'
urlpatterns = [
  path("",home_view,name='home'),
  path("register",registration_view,name='register'),
  path("login",login_view,name='login'),
  path("logout",logout_view,name='logout'),
  path("create",create_view,name='create'),
  path("<int:id>",detail_view,name='detail'),
  path("<int:article_id>/",comment_view,name='comment'),
  # path("<int:delete_id>/",commentdelete_view,name='commentdelete'),
  




]