from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Timestamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Admin(Timestamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='admin')

    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name='admin')
        self.user.groups.add(grp)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Blogger(Timestamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, null=True, blank=True)
    photo = models.ImageField(upload_to='customer', null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name='blogger')
        self.user.groups.add(grp)

        super().save(*args, **kwargs)

class Article(Timestamp):
	title=models.CharField(max_length=200)
	slug=models.SlugField(unique=True)
	image=models.ImageField(upload_to='blogs')
	content=models.TextField()
	date=models.DateTimeField(auto_now_add=True)
	author=models.ForeignKey(User,on_delete=models.CASCADE)
	views=models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.title
	"""docstring for """

class Comment(Timestamp):
    article=models.ForeignKey(Article,on_delete=models.CASCADE)
    commenter=models.ForeignKey(User,on_delete=models.CASCADE)
    text=models.TextField()
    date=models.DateTimeField(auto_now_add=True)
    root=models.ForeignKey("self",on_delete=models.CASCADE,null=True,blank=True)
    #self le chai Comment lai call gareko ho 

    def __str__(self):
        return " comment for "+ self.article.title + " by " + self.commenter.username	 

class ArticleImage(Timestamp):
    article=models.ForeignKey(Article,on_delete=models.CASCADE)
    image=models.ImageField(upload_to="blog/images")
    comment=models.ForeignKey(Comment,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.article.title               