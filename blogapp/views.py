from django.shortcuts import render,redirect
from django.views.generic import *
from django.urls import reverse_lazy, reverse
from django.http import JsonResponse
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from .models import Admin,Blogger,Article,Comment,ArticleImage
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import logout, authenticate, login
from .forms import CreateUserForm,BlogForm,CommentForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required


def home_view(request):
    articlelist=ArticleImage.objects.all()
    context={
             'list':articlelist,
    }


    return render(request, "home.html",context)

def registration_view(request):
    form=CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user=form.cleaned_data.get("username")
            messages.success(request,"Account was successfully registered"+user)

        return redirect('blogapp:login')    

    context={"form":form}
    return render(request,'register.html',context)

def login_view(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    print(username)
    print(password)
    user = authenticate(request,username=username,password=password)
    print(user)
    if user is not None:
        login(request,user)
        print('login successfull')
        return redirect('/')

    return render(request,'login.html')    

def logout_view(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("/")

@login_required
def create_view(request):
    # add the dictionary during initialization 
    form = BlogForm(request.POST,request.FILES or None)
    context={}

    if form.is_valid(): 
        logged_in_user = request.user
        form.instance.author = logged_in_user
        artobj=form.save()

        uploaded_images=request.FILES.getlist("images")

        for img in uploaded_images:
            ArticleImage.objects.create(article=artobj,image=img)
         
        return redirect("/")
          
    context['form']= form 
    return render(request, "create.html", context) 

@login_required
def detail_view(request,id):
    form=CommentForm()
    articlelist=ArticleImage.objects.get(id=id)
    comment=articlelist.comment.all()
    context={
             'detail':articlelist,
             'comments':form,
             'commentss':comment,
    }

    return render(request, "detail.html",context)

@login_required
def comment_view(request,article_id):
    print(article_id)
    if request.method=='POST':
        print(request.POST['text'])
        form=CommentForm(request.POST)
        if form.is_valid():
            form.instance.article = Article.objects.get(id=article_id)
            form.instance.commenter = User.objects.get(username=request.user.username)
            form.save()
            return HttpResponseRedirect(reverse('blogapp:detail',kwargs={'id':article_id}))
        # form.instance.article=Article.objects.get(id=id)
        # form.instance.commenter=User.objects.get(user=request.user)
        # form.instance.text=request.POST['text']

        # form.save()
    else:
        form=CommentForm()
        articlelist=ArticleImage.objects.get(id=id)
        context = {
            'comments':form,
            'detail':articlelist,
        }

    return render(request,"detail.html",context) 

# def commentdelete_view(request,delete_id):
